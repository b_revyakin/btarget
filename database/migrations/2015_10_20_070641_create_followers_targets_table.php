<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFollowersTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('followers_targets', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('target_id')->unsigned();
            $table->foreign('target_id')->references('id')->on('targets');

            $table->integer('follower')->unsigned();
            $table->foreign('follower')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('followers_targets');
    }
}
