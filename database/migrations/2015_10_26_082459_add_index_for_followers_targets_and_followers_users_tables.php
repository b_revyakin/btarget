<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIndexForFollowersTargetsAndFollowersUsersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('followers_users', function (Blueprint $table) {
            $table->unique(['user_id', 'user_follower']);
        });

        Schema::table('followers_targets', function (Blueprint $table) {
            $table->unique(['target_id', 'follower']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('followers_targets', function (Blueprint $table) {
            $table->dropUnique('followers_targets_target_id_follower_unique');
        });

        Schema::table('followers_users', function (Blueprint $table) {
            $table->dropUnique('followers_users_user_id_user_follower_unique');
        });
    }
}
