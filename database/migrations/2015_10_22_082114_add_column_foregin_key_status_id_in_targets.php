<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddColumnForeginKeyStatusIdInTargets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('targets', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->integer('status_id')->unsigned();
            $table->foreign('status_id')->references('id')->on('target_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('targets', function (Blueprint $table) {
            $table->dropForeign('targets_status_id_foreign');
            $table->dropeColumn('status_id');
            $table->tinyInteger('status')->default(1);
        });
    }
}
