<?php

use App\Models\StatusTarget;
use Illuminate\Database\Seeder;

class StatusTargetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusTarget::create([
            'title' => 'wait'
        ]);
        StatusTarget::create([
            'title' => 'block'
        ]);
        StatusTarget::create([
            'title' => 'active'
        ]);

    }
}
