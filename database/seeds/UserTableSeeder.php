<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name' => 'Admin User',
            'username' => 'admin_user',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'admin' => 1,
        ]);

        User::create([
            'name' => 'Test User',
            'username' => 'test_user',
            'email' => 'user@user.com',
            'password' => bcrypt('user'),
        ]);

        factory(App\Models\User::class, 3)->create()->each(function (App\Models\User $u) {
            $u->target()->saveMany(factory(App\Models\Target::class, 30)->make());
        });
    }
}
