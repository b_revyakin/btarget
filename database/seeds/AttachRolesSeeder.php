<?php

use App\Models\User;
use Bican\Roles\Models\Role;
use Illuminate\Database\Seeder;

class AttachRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('slug', 'admin')->first();
        $user = User::where('email', 'admin@admin.com')->first();
        $user->attachRole($role);
    }
}
