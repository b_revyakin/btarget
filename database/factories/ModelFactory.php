<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'username' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt('admin'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Target::class, function (Faker\Generator $faker) {
    return [
        'target_name' => $faker->sentence($nbWords = 3),
        'necessary_cash' => $faker->numberBetween($min = 100, $max = 2000),
        'user_id' => rand(App\Models\User::all()->min('id'), App\Models\User::all()->max('id')),
        'description' => $faker->paragraph($nbSentences = 6),
        'status_id' => App\Models\StatusTarget::where('title', 'wait')->first()->id
    ];
});
