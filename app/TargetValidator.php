<?php

namespace App;

use App\Models\Target;
use Auth;

class TargetValidator extends \Illuminate\Validation\Validator
{

    public function validateUniqueTarget($attribute, $value)
    {
        if (Target::where('user_id', Auth::id())->where('target_name', $value)->exists()) {
            return false;
        }
        return true;
    }
}
