<?php
namespace App;

use App\Models\Photo;
use App\Models\Target;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AddPhotoToTarget
{

    protected $target;
    protected $file;

    public function __construct(Target $target, UploadedFile $file, Thumbnail $thumbnail = null)
    {
        $this->target = $target;
        $this->file = $file;
        $this->thumbnail = $thumbnail ?: new Thumbnail;
    }

    public function save()
    {

        $photo = $this->target->addPhoto($this->makePhoto());

        $this->file->move($photo->baseDir(), $photo->name);

        $this->thumbnail->make($photo->path, $photo->thumbnail_path);
    }

    protected function makePhoto()
    {
        return new Photo(['name' => $this->makeFileName()]);
    }

    protected function makeFileName()
    {
        $name = sha1(
            time() . $this->file->getClientOriginalName()
        );
        $extension = $this->file->getClientOriginalExtension();
        return "{$name}.{$extension}";
    }
}