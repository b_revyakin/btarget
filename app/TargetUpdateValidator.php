<?php

namespace App;

use App\Models\Target;
use Auth;

class TargetUpdateValidator extends \Illuminate\Validation\Validator
{
    public function validateUniqueUpdateTarget($attribute, $value)
    {
        if (Target::where('user_id', Auth::id())->where('id', $_REQUEST['target_id'])->exists()) {
            $targets = Target::where('user_id', Auth::id())->where('id', '<>', $_REQUEST['target_id'])->get();
            foreach ($targets as $target) {
                if ($target->target_name == $value) {
                    return false;
                }
            }
        }
        return true;
    }
}