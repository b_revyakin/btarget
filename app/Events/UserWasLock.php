<?php

namespace App\Events;

use App\Http\Requests;
use App\Models\User;
use Illuminate\Queue\SerializesModels;

class UserWasLock extends Event
{
    use SerializesModels;

    public $user;
    public $data = [];

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data, User $user)
    {
        $this->user = $user;
        $this->data = $data;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
