<?php

namespace App\Listeners;

use App\Events\UserWasUnlock;
use App\Models\User;
use Mail;

class EmailNotificationUnlockForAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserWasUnlock $event
     * @return void
     */
    public function handle(UserWasUnlock $event)
    {
        $users = User::all();
        foreach ($users as $admin) {
            if ($admin->is('admin')) {
                Mail::send('emails.notificate_unlock_admin', ['admin' => $admin, 'user' => $event->user],
                    function ($message) use ($admin) {
                        $message->from('system@gmail.com', 'System BTarget');
                        $message->to($admin->email, $admin->name)->subject('Admin Unlock user.');
                    });
            }
        }
    }
}
