<?php

namespace App\Listeners;

use App\Events\UserWasLock;
use App\Models\User;
use Mail;

class EmailNotificationLockForAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserWasLock $event
     * @return void
     */
    public function handle(UserWasLock $event)
    {
        $users = User::all();
        foreach ($users as $admin) {
            if ($admin->is('admin')) {
                Mail::send('emails.notificate_lock_admin', ['admin' => $admin, 'user' => $event->user, 'info' => $event->data],
                    function ($message) use ($admin) {
                        $message->from('system@gmail.com', 'System BTarget');
                        $message->to($admin->email, $admin->name)->subject('Admin lock user.');
                    });
            }
        }
    }
}
