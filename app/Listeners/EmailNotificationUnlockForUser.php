<?php

namespace App\Listeners;

use App\Events\UserWasUnlock;
use Mail;

class EmailNotificationUnlockForUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserWasUnlock $event
     * @return void
     */
    public function handle(UserWasUnlock $event)
    {
        Mail::send('emails.unlock_user', ['user' => $event->user],
            function ($message) use ($event) {
                $message->from('system@gmail.com', 'System BTarget');
                $message->to($event->user->email, $event->user->name)->subject('You Unlock!');
            });
    }
}
