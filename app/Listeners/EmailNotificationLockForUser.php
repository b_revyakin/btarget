<?php

namespace App\Listeners;

use App\Events\UserWasLock;
use Mail;

class EmailNotificationLockForUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserWasLock $event
     * @return void
     */
    public function handle(UserWasLock $event)
    {
        Mail::send('emails.lock_user', ['user' => $event->user, 'info' => $event->data],
            function ($message) use ($event) {
                $message->from('system@gmail.com', 'System BTarget');
                $message->to($event->user->email, $event->user->name)->subject('You Block!');
            });
    }
}
