<?php

namespace App\Providers;

use App\TargetUpdateValidator;
use App\TargetValidator;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::resolver(function ($translator, $data, $rules, $messages) {
            return new TargetValidator($translator, $data, $rules, $messages);
        });
        Validator::resolver(function ($translator, $data, $rules, $messages) {
            return new TargetUpdateValidator($translator, $data, $rules, $messages);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
