<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Photo extends Model
{
    protected $table = 'target_photos';
    protected $fillable = ['path', 'name', 'thumbnail_path'];


    public function target()
    {
        return $this->belongsTo(Target::class);
    }

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->path = $this->baseDir() . '/' . $name;
        $this->thumbnail_path = $this->baseDir() . '/tn-' . $name;
    }

    public function baseDir()
    {
        return 'images/photos';
    }

    public function upload()
    {
        $this->makeThumbnail();
        return $this;

    }

    protected function makeThumbnail()
    {
        Image::make($this->path)
            ->fit(150)
            ->save($this->thumbnail_path);
    }

    public function delete()
    {
        \File::delete([
            $this->path,
            $this->tumbnail_path

        ]);
        parent::delete();

    }
}
