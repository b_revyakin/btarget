<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Target
 *
 * @property-read \App\Models\User $owner
 */
class Target extends Model
{
    protected $table = 'targets';

    protected $fillable = ['target_name', 'user_id', 'necessary_cash', 'description', 'status_id', 'current_cash'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public static function LocatedAt($user_id, $target_name)
    {
        $target_name = str_replace('-', ' ', $target_name);
        return static::where(compact('user_id', 'target_name'))->firstOrFail();
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function addPhoto(Photo $photo)
    {
        return $this->photos()->save($photo);
    }

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function status()
    {
        return $this->hasOne(StatusTarget::class, 'id', 'status_id');
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers_targets', 'target_id', 'follower')->withTimestamps();
    }


}
