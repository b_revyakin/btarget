<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusTarget extends Model
{
    protected $table = 'target_status';
    protected $fillable = ['id', 'title'];

    public function target()
    {
        return $this->belongsTo(Target::class, 'status_id');
    }
}
