<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;

class UnlockUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'unlock:users {--auto}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command unlock users, if have ban.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $columns = Schema::getColumnListing('users');
        $users = User::where('is_banned', true)->where('banned_until', '<=', Carbon::now())->get();

        if ($this->option('auto')) {
            foreach ($users as $user) {
                $user->is_banned = false;
                $user->banned_until = null;
                $user->save();
            }
        } else {
            if ($users === null) {
                $this->info('Not users blocked administrator');
            } else {
                $this->info('Users have block administrator:');
                $this->table($columns, $users->toArray());

                if ($this->confirm('Unlock all users now? [Y|n]')) {
                    $users = User::where('is_banned', true)->get();
                    foreach ($users as $user) {
                        $user->is_banned = false;
                        $user->banned_until = null;
                        $user->save();
                    }
                    $this->info('All users unlocked.');
                }
            }
        }
    }
}
