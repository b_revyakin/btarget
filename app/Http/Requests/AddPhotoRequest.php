<?php

namespace App\Http\Requests;

use App\Models\Target;

class AddPhotoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Target::where([

            'user_id' => $this->user()->id

        ])->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ];
    }

    public function forbiddenResponse()
    {
        flash()->error('Error', 'You not owner');
        return back();
    }
}
