<?php

namespace App\Http\Requests;

use App\Models\User;
use Auth;

class PasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return password_verify($this->current_password, User::find(Auth::id())->password);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => 'required|min:5',
            'password' => 'required|confirmed|min:5'
        ];
    }

    public function forbiddenResponse()
    {
        flash()->error('Error', 'Not valid password');
        return back();
    }
}
