<?php

namespace App\Http\Requests;

use App\Models\Target;
use Auth;

class TargetRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id = $this->one;
        return Target::where('id', $id)->where(['user_id' => Auth::id()])->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function forbiddenResponse()
    {
        flash()->error('Error', 'Forbidden for you');
        return back();
    }
}
