<?php

namespace App\Http\Requests;

use App\Models\User;
use Auth;

class FollowUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return User::find($this->one)->followers->where('id', Auth::id())->isEmpty();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function forbiddenResponse()
    {
        flash()->error('Error', 'You will follow before!');
        return back();
    }
}
