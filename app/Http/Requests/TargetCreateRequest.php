<?php

namespace App\Http\Requests;

class TargetCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'target_name' => 'required|unique_target|min:5',
            'necessary_cash' => 'required|integer|min:0',
            'description' => 'required'
        ];
    }
}
