<?php

namespace App\Http\Requests;

use App\Models\Target;
use Auth;

class TargetUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id = $this->one;
        return Target::where('id', $id)->where(['user_id' => Auth::id()])->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'target_name' => 'required|unique_update_target|min:5',
            'necessary_cash' => 'required|integer|min:0',
            'description' => 'required'
        ];
    }

    public function forbiddenResponse()
    {
        flash()->error('Error', 'Forbidden for you');
        return back();
    }
}
