<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\TargetCreateRequest;
use App\Http\Requests\TargetRequest;
use App\Http\Requests\TargetUpdateRequest;
use App\Models\StatusTarget;
use App\Models\Target;
use Auth;

class TargetController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getCreateTarget()
    {
        return view('target.create');
    }

    public function postCreateTarget(TargetCreateRequest $request)
    {
        Target::create([
            'target_name' => $request->target_name,
            'user_id' => Auth::id(),
            'necessary_cash' => $request->necessary_cash,
            'description' => $request->description,
            'status_id' => StatusTarget::where('title', 'wait')->first()->id
        ]);
        flash()->success('New target create!', 'show your targets');
        return back();
    }

    public function getListTarget()
    {
        $targets = Target::where('user_id', Auth::user()->id)->orderBy('updated_at', 'desc')->paginate(3);
        return view('target.list', compact('targets'));
    }

    public function getEditTarget(TargetRequest $request)
    {
        $target = Target::where('id', $request->one)->where('user_id', Auth::id())->firstOrFail();
        return view('target.edit', compact('target', 'id'));
    }

    public function getInfo($id)
    {
        $target = Target::where('id', $id)->firstOrFail();
        return view('target.info', compact('target'));
    }

    public function putSaveTarget($id, TargetUpdateRequest $request)
    {
        Target::where('id', $id)
            ->update(['target_name' => $request->target_name,
                'necessary_cash' => $request->necessary_cash,
                'description' => $request->description,
                'status_id' => StatusTarget::where('title', 'wait')->first()->id
            ]);
        flash()->success('Success', 'Target save');
        return back();
    }

    public function deleteTarget(TargetRequest $request)
    {
        Target::where('id', $request->one)->delete();
        return redirect()->route('target.list');
    }
}
