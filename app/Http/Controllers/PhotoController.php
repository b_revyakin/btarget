<?php

namespace App\Http\Controllers;

use App\AddPhotoToTarget;
use App\Http\Requests;
use App\Http\Requests\AddPhotoRequest;
use App\Models\Photo;
use App\Models\Target;

class PhotoController extends Controller
{
    public function postAddPhotoTarget($user_id, $target_name, AddPhotoRequest $request)
    {

        $target = Target::LocatedAt($user_id, $target_name);
        $photo = $request->file('photo');
        (new AddPhotoToTarget($target, $photo))->save();

    }

    public function deletePhoto($id)
    {
        Photo::findOrFail($id)->delete();
        return back();
    }
}
