<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\FollowTargetRequest;
use App\Http\Requests\FollowUserRequest;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\ProfileUpdateRequest;
use App\Models\Target;
use App\Models\User;
use Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getInfo()
    {
        $user = User::where('id', Auth::id())->firstOrFail();
        return view('profile.info', compact('user'));
    }

    public function getStatistic()
    {

        $sum = 0;
        $active = 0;
        $wait = 0;
        $not_active = 0;
        $count = 0;
        $targets = User::find(Auth::id())->target;
        if (!empty($targets)) {
            $count = count($targets);
            foreach ($targets as $target) {
                $sum += $target->necessary_cash;

                switch ($target->status->title) {
                    case "block":
                        $not_active++;
                        break;
                    case "active":
                        $active++;
                        break;
                    case "wait":
                        $wait++;
                        break;
                }
            }
        }
        $statistic = ['sum' => $sum, 'active' => $active, 'wait' => $wait, 'not_active' => $not_active, 'count' => $count];
        return view('profile.statistic', compact('targets', 'statistic'));
    }

    public function getEdit()
    {
        $user = User::where('id', Auth::id())->firstOrFail();
        return view('profile.edit', compact('user'));
    }

    public function putSave(ProfileUpdateRequest $request)
    {
        User::where('id', Auth::id())->update([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
        ]);
        flash()->success('success', 'Changes save');
        return redirect()->back();
    }

    public function putChangePassword(PasswordRequest $request)
    {
        User::find(Auth::id())->update([
            'password' => bcrypt($request->password)
        ]);
        flash()->success('success', 'Password change');
        return back();
    }

    public function getUsers()
    {
        $users = User::where('id', '<>', Auth::id())->paginate(15);
        return view('profile.users', compact('users'));
    }

    public function getTargets()
    {
        $targets = Target::where('user_id', '<>', Auth::id())->paginate(15);
        return view('profile.targets', compact('targets'));
    }

    public function getTargetInfo($id)
    {
        $target = Target::find($id);
        $owner = User::find(Target::find($id)->user_id);
        return view('profile.info_target', compact('target', 'owner'));
    }

    public function postFollowTarget(FollowTargetRequest $request)
    {
        Target::find($request->one)->followers()->attach(Auth::id());
        flash()->success('success', 'You follow');
        return redirect()->back();
    }

    public function deleteUnfollowTarget($id)
    {
        Target::find($id)->followers()->detach(Auth::id());
        return redirect()->back();
    }


    public function postFollowUser(FollowUserRequest $request)
    {
            User::find($request->one)->followers()->attach(Auth::id());
            flash()->success('success', 'You follow');
            return redirect()->back();
    }

    public function deleteUnfollowUser($id)
    {
        User::find($id)->followers()->detach(Auth::id());
        return back();
    }

    public function getListIFollowers()
    {
        $users = User::find(Auth::id())->ifollower;
        $targets = User::find(Auth::id())->ifollowerTarget;
        return view('profile.list_followers', compact('users', 'targets'));
    }

    public function getListMyFollowers()
    {
        $users = User::find(Auth::id())->followers;
        return view('profile.list_my_follows', compact('users'));
    }

    public function deleteUnfollowFromUser($id)
    {
        User::find(Auth::id())->followers()->detach($id);
        return back();
    }
}
