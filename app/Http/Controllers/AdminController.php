<?php

namespace App\Http\Controllers;

use App\Events\UserWasLock;
use App\Events\UserWasUnlock;
use App\Http\Requests;
use App\Models\StatusTarget;
use App\Models\Target;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function getDashboard()
    {
        return view('admin.dashboard');
    }

    public function getUsers()
    {
        $users = User::paginate(3);
        return view('admin.users', compact('users'));
    }

    public function getUserInfo($id)
    {
        $user = User::where('id', $id)->firstOrFail();
        return view('admin.user_info', compact('user'));
    }

    public function getTargetsUser($id)
    {
        $targets = Target::where('user_id', $id)->paginate(3);
        return view('admin.user_targets', compact('targets'));
    }

    public function getTargets()
    {
        $targets = Target::orderBy('updated_at', 'desc')->paginate(15)->groupBy('status_id');
//        dd($targets[6]);
        return view('admin.targets', compact('targets'));
    }

    public function putTargetBlock($id)
    {
        Target::where('id', $id)->update(['status_id' => StatusTarget::where('title', 'block')->first()->id]);
        flash()->message('Change', 'Target block.');
        return redirect()->back();
    }

    public function putTargetApply($id)
    {
        Target::where('id', $id)->update(['status_id' => StatusTarget::where('title', 'active')->first()->id]);
        flash()->message('Change', 'Target apply.');
        return redirect()->back();
    }

    public function putBannedUser($id, Request $request)
    {

        User::where('id', $id)->update([
            'is_banned' => true,
            'banned_until' => $request->date
        ]);
        $user = User::find($id);

        event(new UserWasLock($request->all(), $user));

        flash()->message('User lock', $user->name.' is lock to '.$user->banned_until);
        return back();
    }

    public function putUnlockUser($id)
    {
        User::where('id', $id)->update([
            'is_banned' => false,
            'banned_until' => null
        ]);
        $user = User::find($id);

        event(new UserWasUnlock($user));

        flash()->message('Unlock', 'User ' . $user->name . ' is unlock.');
        return back();
    }

    public function getBanForm($id)
    {
        $user = User::find($id);
        return view('admin.ban_form', compact('user'));

    }
}
