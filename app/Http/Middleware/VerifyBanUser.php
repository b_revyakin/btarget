<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class VerifyBanUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if (Auth::attempt(['email' => $request->email,
                'password' => $request->password,
                'is_banned' => true]) || Auth::user() && Auth::user()->is_banned
        ) {
            flash()->message('You is banned', 'Admin banned your account.');
            Auth::logout();
            return redirect()->route('home');
        }
        return $next($request);
    }
}
