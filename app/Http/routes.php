<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', function () {
    return view('layout');
}]);
Route::group(['middleware' => 'ban_verify'], function () {
    Route::controller('auth', 'Auth\AuthController', [
        'getLogin' => 'auth.login',
        'postLogin' => 'auth.login',
        'getLogout' => 'auth.logout',
        'getRegister' => 'auth.register',
        'postRegister' => 'auth.register'
    ]);
    Route::controller('target', 'TargetController', [
        'getListTarget' => 'target.list',
        'getInfo' => 'target.info',
        'postCreateTarget' => 'target.create',
        'getCreateTarget' => 'target.create',
        'getEditTarget' => 'target.edit',
        'putSaveTarget' => 'target.save',
        'deleteTarget' => 'target.delete'
    ]);
    Route::controller('profile', 'ProfileController', [
        'getInfo' => 'profile.info',
        'getEdit' => 'profile.edit',
        'putSave' => 'profile.save',
        'getStatistic' => 'profile.statistic',
        'getUsers' => 'profile.users',
        'getTargets' => 'profile.targets',
        'getTargetInfo' => 'profile.info_target',
        'postFollowTarget' => 'profile.follow_target',
        'deleteUnfollowTarget' => 'profile.unfollow_target',
        'postFollowUser' => 'profile.follow_user',
        'deleteUnfollowUser' => 'profile.unfollow_user',
        'getListIFollowers' => 'profile.list',
        'getListMyFollowers' => 'profile.list_my_follows',
        'putChangePassword' => 'profile.change_password',
        'deleteUnfollowFromUser' => 'profile.unfollow_from_user'
    ]);
    Route::controller('photo', 'PhotoController', [
        'postAddPhotoTarget' => 'photo.target_photo',
        'deletePhoto' => 'photo.delete'
    ]);
});
Route::group(['middleware' => 'role:admin'], function () {
    Route::controller('admin', 'AdminController', [
        'getDashboard' => 'admin.dashboard',
        'getUsers' => 'admin.users',
        'getUserInfo' => 'admin.user_info',
        'getTargetsUser' => 'admin.user_targets',
        'getTargets' => 'admin.targets',
        'putTargetBlock' => 'admin.target_block',
        'putTargetApply' => 'admin.target_apply',
        'putBannedUser' => 'admin.banned',
        'putUnlockUser' => 'admin.unlock_user',
        'getBanForm' => 'admin.ban_form'


    ]);
});
