@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>This Account is Blocked by Administrator!</li>
            @endforeach
        </ul>
    </div>
@endif