@extends('layout')
@section('content.container')
    <div class="sidebar-content  gray-bg col-md-6 col-lg-offset-1">

    <div class="jumbotron">
            <h2>Hello, {{ $user->name }}!</h2>

            <h3>Information:</h3>

            <p>Name: {{ $user->username }}</p>

            <p>Email: {{ $user->email }}</p>

            <p><a class="btn btn-primary btn-lg" href="{{ route('profile.edit') }}" role="button">Edit</a></p>
        </div>

    </div>

@endsection