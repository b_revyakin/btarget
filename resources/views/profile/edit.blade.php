@extends('layout')
@section('content.container')

    <div class="well container col-md-6 col-md-offset-3">

        @include('partials.errors')

        <h1>Edit profile</h1>
        <form class="form-group" method="POST" action="{{ route('profile.save') }}">

            <input type="hidden" name="_method" value="PUT">
            {!! csrf_field() !!}


            <div class="form-group">
                <label for="name">Nickname:</label>
                <input type="text" name="name" class="form-control"
                       value="{{ empty(old('name')) ? $user->name : old('name') }}"
                       required autofocus>
            </div>

            <div class="form-group">
                <label for="username">Name:</label>
                <input type="text" name="username" class="form-control"
                       value="{{ empty(old('username')) ? $user->username : old('username') }}"
                       required>
            </div>

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" name="email" class="form-control"
                       value="{{ empty(old('email')) ? $user->email : old('email')}}"
                       required>
            </div>

            <div class="form-group">
                <button class="btn btn-lg btn-success btn-block" type="submit">Save</button>
            </div>

        </form>

        <h1>Change password</h1>

        <form class="form-group" method="POST" action="{{ route('profile.change_password') }}">

            <input type="hidden" name="_method" value="PUT">
            {!! csrf_field() !!}

            <div class="form-group">
                <label for="current_password">Current password:</label>
                <input type="password" name="current_password" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="password">New password:</label>
                <input type="password" name="password" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="password_confirmation">Confirm new password:</label>
                <input type="password" name="password_confirmation" class="form-control" required>
            </div>

            <div class="form-group">
                <button class="btn btn-lg btn-success btn-block" type="submit">Change Password</button>
            </div>

        </form>

    </div>

@stop