@extends('layout')
@section('content.container')
    @parent
    <div class="table-responsive col-md-8 well">
        <h1>Users</h1>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Nickname</th>
                <th>Email</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        <form method="POST" action="{{ route('profile.unfollow_user' , $user->id) }}">
                            <input type="hidden" name="_method" value="DELETE">
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-danger">Unfollow</button>
                        </form>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
        <h1>Targets</h1>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Need Cash</th>
                <th>Current Cash</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($targets as $target)
                <tr>
                    <td>{{ $target->target_name }}</td>
                    <td>{{ $target->necessary_cash }}</td>
                    <td>{{ $target->current_cash }}</td>
                    <td>
                        <form method="POST" action="{{ route('profile.unfollow_target', $target->id) }}">
                            <input type="hidden" name="_method" value="DELETE">
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-danger">Unfollow</button>
                        </form>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@stop