@extends('layout')
@section('content.container')
    @parent
    <div class="table-responsive col-md-8 well">
        <h1>Users</h1>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Nickname</th>
                <th>Email</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        <form method="POST" action="{{ route('profile.unfollow_from_user' , $user->id) }}">
                            <input type="hidden" name="_method" value="DELETE">
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-danger">Delete Follower</button>
                        </form>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

@stop