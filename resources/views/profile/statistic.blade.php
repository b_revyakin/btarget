@extends('layout')
@section('content.container')
    @parent
    <div class="container well col-md-8 col-lg-offset-2">

        <h2>Table info about my Targets</h2>

        <div class="alert alert-info">

            <li>Need cash for targets:{{ $statistic['sum'] }}</li>

            <li>Total my targets:{{ $statistic['count'] }}</li>

            <li>Activety targets:{{ $statistic['active'] }}</li>

            <li>Not Activety targets:{{ $statistic['not_active'] }}</li>

            <li>Wait Apply targets:{{ $statistic['wait'] }}</li>

        </div>

        <div class="table-responsive">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Cash</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach($targets as $target)
                    <tr>
                        <td>{{ $target->target_name }}</td>
                        <td>{{ $target->necessary_cash }}</td>
                        <td>{{ $target->status->title }}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection