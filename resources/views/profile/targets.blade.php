@extends('layout')
@section('content.container')
    @parent
    @include('partials.sidebar')
    <div class="table-responsive well col-md-8">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Need Cash</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($targets as $target)
                <tr>
                    <td>{{ $target->target_name }}</td>
                    <td>{{ $target->necessary_cash }}</td>
                    <td style="width: 640px">{{ $target->description }}</td>
                    <td>
                        <a class="btn btn-info" href="{{ route('profile.info_target', $target->id) }}">Info</a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="container" align="center">
        {!! $targets->render() !!}
    </div>

@stop