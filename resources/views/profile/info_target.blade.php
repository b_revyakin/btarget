@extends('layout')
@section('content.container')
    @parent
    @include('partials.sidebar')
        <div class="container well col-md-8">
            <h1>Targets Name: {{ $target->target_name }}</h1>

            <h3>Owner: {{ $owner->name }} Email: {{ $owner->email }}</h3>
            <strong>Need Cash: {{ $target->necessary_cash }} Current Cash: {{ $target->current_cash }}</strong>

            <p>Description:{{ $target->description }}</p>
            @if ($target->followers->where('id', Auth::id())->isEmpty())
                <form method="POST" action="{{ route('profile.follow_target', $target->id) }}">
                    {!! csrf_field() !!}
                    <button type="submit" class="btn btn-success">Follow</button>
                </form>
            @endif
        </div>
@stop