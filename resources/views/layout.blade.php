<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Home</title>

    <!-- Bootstrap -->
    <link href="/components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    {{--Dropzone--}}
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/dropzone.css">--}}
    <link rel="stylesheet" href="/components/enyo/dropzone/dist/dropzone.css">
    {{--Sweetalert--}}
    <link rel="stylesheet" href="/components/sweetalert/dist/sweetalert.css">
    {{--Fond-awesome for icons--}}
    <link href="/font-awesome-4.4.0/css/font-awesome.css" rel="stylesheet">
    {{--INSPINIA--}}
    <link href="/inspinia/css/animate.css" rel="stylesheet">
    <link href="/inspinia/css/style.css" rel="stylesheet">

</head>
<body class="fixed-navigation">

@include('nav')
@yield('content')
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/components/jquery/dist/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="/components/bootstrap/dist/js/bootstrap.min.js"></script>
{{--Dropzone--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/dropzone.js"></script>--}}
<script src="/components/enyo/dropzone/dist/dropzone.js"></script>
{{--Sweetalert--}}
<script src="/components/sweetalert/dist/sweetalert.min.js"></script>

{{--INSPINIA--}}
<!-- Mainly scripts -->
<script src="/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>

{{--<!-- Custom and plugin javascript -->--}}
<script src="/inspinia/js/inspinia.js"></script>
<script src="/inspinia/js/plugins/pace/pace.min.js"></script>

@yield('scripts.footer')

@include('flash')
</body>

</html>
