<div class="well col-md-8 col-lg-offset-2">
    <h1>By {{ $user->name }} , you are locked by the administrator on the site BTarget.</h1>

    <p><strong>Description cause: </strong>{{ $info['description'] }}</p>

    <p>System auto unlock you at {{ $info['date'] }}</p>
    <hr>
</div>
