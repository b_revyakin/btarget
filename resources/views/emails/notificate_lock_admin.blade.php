<div class="well col-md-8 col-lg-offset-2">
    <h3>User {{ $user->name }} , will lock by the administrator on the site BTarget.</h3>

    <p><strong>Info about admin: </strong>Email: {{ $admin->email }}, Name: {{ $admin->name }}</p>

    <p><strong>Where: {{ $user->updated_at }}</strong></p>

    <p><strong>Description cause: </strong>{{ $info['description'] }}</p>

    <p><strong>Locked until: </strong>{{ $info['date'] }}</p>
    <hr>
</div>