@extends('layout')
@section('content')
    @parent
    <div class="wrapper wrapper-content animated fadeInRight gray-bg">
        <div class="row col-md-offset-3">
            <div class="col-lg-8">
                @include('partials.errors')
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Login
                            <small>Simple login for Btarget</small>
                        </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-6 b-r"><h3 class="m-t-none m-b">Sign in</h3>
                                <p>Sign in today for more expirience.</p>
                                <form role="form" method="POST" action="{{ route('auth.login') }}">
                                    {!! csrf_field() !!}
                                    <div class="form-group"><label>Email</label> <input type="email" name="email"
                                                                                        placeholder="Enter email"
                                                                                        class="form-control"
                                                                                        value="{{ old('email') }}"
                                                                                        required autofocus></div>
                                    <div class="form-group"><label>Password</label> <input type="password"
                                                                                           name="password"
                                                                                           placeholder="Password"
                                                                                           class="form-control"
                                                                                           required></div>
                                    <div>
                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit">
                                            <strong>Log in</strong></button>
                                        <label> <input type="checkbox" class="i-checks"> Remember me </label>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-6"><h4>Not a member?</h4>
                                <p>You can create an account:</p>
                                <p class="text-center">
                                    <a href="{{ route('auth.register') }}"><i class="fa fa-sign-in big-icon"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
