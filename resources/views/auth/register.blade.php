@extends('layout')
@section('content')
    @parent
    <div class="wrapper wrapper-content animated fadeInRight gray-bg">
        <div class="row col-md-offset-3">
            <div class="col-lg-8">
                @include('partials.errors')
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Registration
                            <small>Simple registration for Btarget.</small>
                        </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-6 b-r"><h3 class="m-t-none m-b">Sign up</h3>
                                <form class="form-group" method="POST" action="{{ route('auth.register') }}">
                                    {!! csrf_field() !!}
                                    <div class="form-group">
                                        <label for="name">Nickname:</label>
                                        <input type="text" name="name" class="form-control" value="{{ old('name') }}"
                                               required autofocus>
                                    </div>

                                    <div class="form-group">
                                        <label for="username">User Name:</label>
                                        <input type="text" name="username" class="form-control"
                                               value="{{ old('username') }}"
                                               required>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email address:</label>
                                        <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                                               required>
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password:</label>
                                        <input type="password" name="password" class="form-control" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="password_confirmation">Confirm Password:</label>
                                        <input type="password" name="password_confirmation" class="form-control"
                                               required>
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit">
                                            Registration
                                        </button>
                                    </div>

                                </form>
                            </div>
                            <div class="col-sm-6"><h4>Already have an account ?</h4>
                                <p>You can sign in:</p>
                                <p class="text-center">
                                    <a href="{{ route('auth.login') }}"><i class="fa fa-sign-in big-icon"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection