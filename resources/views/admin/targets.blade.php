@extends('layout')
@section('content.container')
    @parent
    <div class="table-responsive well">
        <h1 align="center">Targets list</h1>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Targets Name</th>
                <th>Need cash</th>
                <th>Description</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($targets as $target)
                <tr>
                    <td>{{ $target->target_name }}</td>
                    <td>{{ $target->necessary_cash }}</td>
                    <td><p>{{ $target->description }}</p></td>
                    <td>{{ $target->status->title }}</td>
                    <td>
                        <form method="POST" action="{{ route('admin.target_apply', $target->id) }}">
                            <input type="hidden" name="_method" value="PUT">
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-success">Apply</button>
                        </form>
                        <form method="POST" action="{{ route('admin.target_block', $target->id) }}">
                            <input type="hidden" name="_method" value="PUT">
                            {!! csrf_field() !!}
                            <button type="submit" class="btn btn-danger">Block</button>
                        </form>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="container" align="center">
        {!! $targets->render() !!}
    </div>
@stop
