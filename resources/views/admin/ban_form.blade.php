@extends('layout')
@section('content.container')
    @parent
    <div>
        <div class="container well col-md-8">
            <h3>Form banned user</h3>
            <h4>User name: {{ $user->name }}</h4>
            <h4>Email: {{ $user->email }}</h4>

            <form method="POST" action="{{ route('admin.banned', $user->id) }}">
                <input type="hidden" name="_method" value="PUT">
                {!! csrf_field() !!}

                <div class="form-group">
                    <label for="description">Description cause:</label>
                    <textarea type="text" name="description" class="form-control" required></textarea>
                </div>

                <div class="form-group">
                    <label for="date">Banned until:</label>
                    <input type="date" name="date" class="form-control" required>
                </div>
                <button type="submit" class="btn btn-danger">Banned</button>
            </form>
        </div>
    </div>
@stop