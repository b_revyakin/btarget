@extends('layout')
@section('content.container')
    @parent
    <div class="table-responsive well">
        <h1 align="center">Users list</h1>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Nickname</th>
                <th>User Name</th>
                <th>Email</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->email }}</td>
                    <td><a class="btn btn-info" href="{{ route('admin.user_info', $user->id) }}"
                           role="button">Info</a>
                        <a class="btn btn-success" href="{{ route('admin.user_targets', $user->id) }}"
                           role="button">Targets</a>
                        @if (!$user->is_banned)
                            <a class="btn btn-danger" href="{{ route('admin.ban_form', $user->id) }}"
                               role="button">Banned</a>
                        @else
                            <form method="POST" action="{{ route('admin.unlock_user', $user->id) }}">
                                <input type="hidden" name="_method" value="PUT">
                                {!! csrf_field() !!}
                                <button type="submit" class="btn btn-success">Unlock</button>
                            </form>
                        @endif
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="container" align="center">
        {!! $users->render() !!}
    </div>

@stop
