@extends('layout')
@section('content.container')
    @parent
    <div class="container col-md-6 col-lg-offset-3">

        <div class="jumbotron">
            <h2>{{ $user->name }}</h2>

            <h3>Information:</h3>

            <p>Name: {{ $user->username }}</p>

            <p>Email: {{ $user->email }}</p>
        </div>

    </div>
@endsection