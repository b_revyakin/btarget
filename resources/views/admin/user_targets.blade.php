@extends('layout')

@section('content.container')
    @parent
    <div class="container well">

        <h1>User Targets:</h1>
        @foreach($targets as $target)
            <div class="col-md-4">
                <h2>{{$target->target_name}}</h2>

                <p>{{$target->description}}</p>
                <h4>{{$target->cash}}</h4>

            </div>
        @endforeach

    </div>

    <div class="container" align="center">
        {!! $targets->render() !!}
    </div>
@stop