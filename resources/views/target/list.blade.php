@extends('layout')

@section('content.container')
    @parent
    <div class="container well">
        <h1>My Targets</h1>
        @foreach($targets as $target)
            <div class="col-md-4">
                <h2>{{$target->target_name}}</h2>

                <p>{{$target->description}}</p>
                <h4>{{$target->necessary_cash}}</h4>

                <p><a class="btn btn-info" href="{{ route('target.info', $target->id) }}"
                      role="button">Info</a>
                    <a class="btn btn-default" href="{{ route('target.edit', $target->id) }}"
                       role="button">Edit &raquo;</a>
                </p>
            </div>
        @endforeach
    </div>
    <div class="container" align="center">
        {!! $targets->render() !!}
    </div>
@stop