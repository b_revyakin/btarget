@extends('layout')
@section('content.container')

    <div class="col-md-8 col-md-offset-2 well">

        @include('partials.errors')

        <form class="form-group" method="POST" action="{{ route('target.save', $target->id) }}">
            <input type="hidden" name="_method" value="PUT">
            {!! csrf_field() !!}
            <h2 class="form-group">Target</h2>
            <input type="text" class="form-control hidden" name="target_id" value="{{ $target->id }}">
            <div class="form-group">
                <label for="target_name">Name:</label>
                <input type="text" name="target_name" class="form-control"
                       value="{{ empty(old('target_name')) ? $target->target_name : old('target_name') }}"
                       required autofocus>
            </div>

            <div class="form-group">
                <label for="necessary_cash">Need Cash:</label>
                <input type="text" name="necessary_cash" class="form-control"
                       value="{{ empty(old('necessary_cash')) ? $target->necessary_cash : old('necessary_cash') }}"
                       required>
            </div>

            <div class="form-group">
                <label for="description">Description:</label>
                <textarea type="text" name="description" class="form-control"
                          required>{{  empty(old('description')) ? $target->description : old('description') }}</textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-lg btn-success btn-block" type="submit">Save</button>
            </div>
        </form>

        <form method="POST" action="{{ route('target.delete', $target->id) }}">
            <input type="hidden" name="_method" value="DELETE">
            {!! csrf_field() !!}
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>
    </div>

@stop