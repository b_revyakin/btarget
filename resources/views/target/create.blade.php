@extends('layout')
@section('content.container')
    @parent
    <div class="col-md-8 col-md-offset-2 well">

        @include('partials.errors')

        <form class="form-group" method="POST" action="{{ route('target.create') }}">
            {!! csrf_field() !!}
            <h2 class="form-group">Create Target</h2>

            <div class="form-group">
                <label for="target_name">Name:</label>
                <input type="text" name="target_name" class="form-control"
                       value="{{ old('target_name') }}"
                       required autofocus>
            </div>

            <div class="form-group">
                <label for="necessary_cash">Need Cash:</label>
                <input type="text" name="necessary_cash" class="form-control" value="{{ old('necessary_cash') }}"
                       required>
            </div>

            <div class="form-group">
                <label for="description">Description:</label>
                <textarea type="text" name="description" class="form-control"
                          required>{{ old('description') }}</textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Create</button>
            </div>

        </form>
    </div>
@endsection