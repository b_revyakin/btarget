@extends('layout')
@section('content.container')
    @parent
    <div class="container">
        <div class="well col-md-8 col-lg-offset-2">
            <h1>Targets Name: {{ $target->target_name }}</h1>
            <strong>Need Cash: {{ $target->necessary_cash }} Current Cash: {{ $target->current_cash }}</strong>

            <p>Description:{{ $target->description }}</p>
            <hr>
            <h1>Photo:</h1>

            <div class="gallery well">
                @foreach($target->photos->chunk(4) as $set)
                    <div class="row">
                        @foreach($set as $photo)
                            <div class="col-md-3 gallery_images">
                                <form method="POST" action="{{ route('photo.delete', $photo->id) }}">
                                    <input type="hidden" name="_method" value="DELETE">
                                    {!! csrf_field() !!}
                                    <button class="btn btn-danger btn-xs" type="submit"><span
                                                class="glyphicon glyphicon-remove"></span></button>
                                </form>
                                <img src="/{{$photo->thumbnail_path}}">
                            </div>
                        @endforeach

                    </div>
                @endforeach
            </div>
            <form id="addPhotosForm"
                  action="{{route('photo.target_photo', [$target->user_id, $target->target_name])}}"
                  method="POST" class="dropzone">
                {{csrf_field()}}
            </form>

            <div id="disqus_thread"></div>
        </div>

    </div>
@stop
@section('scripts.footer')

    <script src="/js/dropezone_bar.js"></script>

    <script src="/js/disqus.js"></script>

    <script id="dsq-count-scr" src="//btarget.disqus.com/count.js" async></script>

@stop

