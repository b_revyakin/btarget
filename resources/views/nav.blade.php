@if (!Auth::guest())
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="">
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"><strong
                                            class="font-bold">{{ Auth::user()->name }}</strong>
                             </span> <span class="text-muted text-xs block">{{Auth::user()->is('admin') ? 'Admin' :'User'}}
                                    <b
                                            class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="{{ route('profile.info') }}">Profile</a></li>
                            <li><a href="#">Change avatar</a></li>
                            <li><a href="{{ route('profile.list_my_follows') }}">My followers</a></li>
                            <li><a href="{{ route('profile.statistic') }}">Statistic</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        BT
                    </div>
                </li>
                <li>
                    <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Users</span> <span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('profile.users') }}">All users</a></li>
                        <li><a href="{{ route('profile.list') }}">I follow</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Targets</span> <span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="{{ route('target.create') }}">Create Target</a></li>
                        <li><a href="{{ route('target.list') }}">My Targets</a></li>
                        <li><a href="{{ route('target.list') }}">All Targets</a></li>
                    </ul>
                </li>
                @role('admin')
                    <li>
                        <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Panel Admin</span> <span
                                    class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{ route('admin.users') }}">Users</a></li>
                            <li><a href="{{ route('admin.targets') }}">Targets</a></li>
                        </ul>
                    </li>
                @endrole
            </ul>
        </div>
    </nav>
@endif

<div id="{{ Auth::guest() ? '' : 'page-wrapper' }}" class="gray-bg sidebar-content">
    <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary {{ Auth::guest() ? 'hidden' : '' }}"
                   href="#"><i class="fa fa-bars"></i> </a>
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="{{route('home')}}"><span
                            class="glyphicon glyphicon-home" aria-hidden="true"></span></a>

                <form role="search" class="navbar-form-custom" method="post" action="#">
                    <div class="form-group">
                        <input type="text" placeholder="Search for something..." class="form-control" name="top-search"
                               id="top-search">
                    </div>
                </form>
            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome to BTarget.</span>
                </li>
                @if (Auth::guest())
                    <li>
                        <a href="{{ route('auth.login') }}">
                            <i class="fa fa-sign-in"></i> Log in
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('auth.register') }}">
                            <i class="fa fa-sign-in"></i> Registration
                        </a>
                    </li>
                @else
                    <li>
                        <a href="{{ route('auth.logout') }}">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                @endif
            </ul>
        </nav>
        @yield('content.container')
        @include('footer')
    </div>
</div>
